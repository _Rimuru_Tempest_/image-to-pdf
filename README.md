# image_to_pdf
Bash script that automates the process of converting images to pdf.

## Functionality
The script can put all your images in one pdf or
The script can put all your images in separate pdfs

## Dependencies
ImageMagic is used for the conversion and the script will attempt to install this software if it isn't present on the system.