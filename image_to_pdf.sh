#!/bin/bash

#**************************************************************#
# Author: Ahmed Bejtovic                                       #
# Usage: Put all your images in the same folder                #
# where this script is located and run the script.             #
#**************************************************************#

_RED='\033[0;31m'
_YELLOW='\033[0;33m'
_GREEN='\033[0;32m'
_BLUE='\033[0;34m'
_NO_COLOR='\033[0m'

_MISSING_DEPENCENCY=10
_DEPENDENCY_EXISTS=0
_ANSWER_YES=0
_ANSWER_NO=1
_CFG_FILE_PATH=""
_CFG_FILE_UPDATED=0
_CFG_FILE_NOT_UPDATED=1
_IS_CFG_FILE_UPDATED=$_CFG_FILE_NOT_UPDATED

_IMAGE_FILES=()
_IMAGE_FILE_TYPE=(PNG JPEG TIFF BMP GIF)

function GetFiles(){
	_type=""
	for f in $(ls -A); do
		_type=$(file $f | cut -d ":" -f 2 | cut -d " " -f2)
		for t in ${_IMAGE_FILE_TYPE[@]}; do
			if [ ${_type^^} = ${t^^} ]; then
				_IMAGE_FILES+=$(file $f | cut -d ":" -f 1)
				_IMAGE_FILES+=" "
			fi
		done
	 done
}

function AskForUserConsent(){
        PS3="Choose an option: "
        select answer in yes no;
        do
                if [ -z $answer ]; then
                        echo "Invalid selection, please try again"
                else
                        break
                fi
        done
	if [ $answer = "yes" ]; then
		return $_ANSWER_YES
	else
		return $_ANSWER_NO
	fi
}

function CheckForDependecies(){
	which convert 1>/dev/null 2>&1 && result=$_DEPENDENCY_EXISTS || result=$_MISSING_DEPENCENCY 1>/dev/null 2>&1
	return $result
}

function InstallDependencies(){
	echo -e "${_RED}[-] imagemagick is not installed on this PC and the script cannot work without it"
	echo -e "${_YELLOW}[*] Do you want to intall imagemagick?(note this requires root privileges): ${_NO_COLOR}"

	AskForUserConsent

	if [ $? -eq $_ANSWER_YES ]; then
		sudo apt install imagemagick -y 1>/dev/null 2>&1 || sudo pacman -S imagemagick 1>/dev/null 2>&1 || sudo dnf install imagemagick -y  1>/dev/null 2>&1
	else
		exit 1
	fi
}

function SetCfgFilePath(){
	dir_path=$(find /etc -iname imagemagick* -type d 2>/dev/null)
	_CFG_FILE_PATH="${dir_path}/policy.xml"
}

function CheckCfgFile(){
	grep_output=$(grep '<policy domain="coder" rights="read|write" pattern="PDF" />' "${_CFG_FILE_PATH}")
	if [ -z ${grep_output} ]; then
		UpdateCfgFile
	fi
}


function UpdateCfgFile(){
	echo -e "${_RED}[-] Note the script needs to make a temperary change in ${_CFG_FILE_PATH}"
	echo -e "${_YELLOW}<policy domain=\"coder\" rights=\"none\" pattern=\"PDF\" /> ->  <policy domain=\"coder\" rights=\"read|write\" pattern=\"PDF\" />"
	echo -e "[*] All changes will be reverted when the script finishes with executing"
	echo -e "[*] Do you want to proceed?(note this requires root privileges): ${_NO_COLOR}"

	AskForUserConsent
	if [ $? -eq $_ANSWER_YES ]; then
	        sudo sed -i 's.<policy domain="coder" rights="none" pattern="PDF" />.<policy domain="coder" rights="read|write" pattern="PDF" />.' "${_CFG_FILE_PATH}"
		_IS_CFG_FILE_UPDATED=$_CFG_FILE_UPDATED
	else
		exit 1
	fi
}

function RevertCfgFile(){
	echo -e "${_BLUE}[*] Reverting changes made in ${_CFG_FILE_PATH}${_NO_COLOR}"
	sudo sed -i 's.<policy domain="coder" rights="read|write" pattern="PDF" />.<policy domain="coder" rights="none" pattern="PDF" />.' "${_CFG_FILE_PATH}"

}

function Convert(){
	PS3="How do you want your files to be converted?: "
	select conversion_type in "All in one file" "In seperate files"; do
		if [ -z "$conversion_type"  ]; then
			echo "Invalid selection, please try again"
		else
			break
		fi
	done

	if [ "$conversion_type" = "All in one file" ]; then
		convert ${_IMAGE_FILES[@]} output.pdf
	else
		for picture in $_IMAGE_FILES; do
			convert "${picture}" "${picture}.pdf"
		done
	fi
}



CheckForDependecies
if [ $? -eq $_MISSING_DEPENCENCY ]; then
	InstallDependencies
fi

SetCfgFilePath
CheckCfgFile
GetFiles
Convert
if [ $_IS_CFG_FILE_UPDATED -eq  $_CFG_FILE_UPDATED ]; then
	RevertCfgFile
fi
